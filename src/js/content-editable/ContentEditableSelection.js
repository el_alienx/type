import TextEditorStore from '../../stores/TextEditorStore'

class ContentEditableSelection {
  constructor (contentEditableInstance) {
    this._contentEditable = contentEditableInstance

    // For Text Editor Store
    this._selection = {}
    this._selectionIndex = 0
    this._selectionFormat = {}
    this._selectionLength = 0
    this._selectionText = ''
    this._selectionUnit = ''
    this._node = {}
    this._nodeIndex = ''
    this._nodeHierarchy = {}
    this._nodeLength = 0
    this._nodeText = 0

    // For File Generation
    this._content = {}

    // Helper
    this._event = {}
  }

  // Event
  get event () {
    return this._event
  }

  set event (event) {
    this._event = event
  }

  // Selection
  get selection () {
    this._selection = {
      index: this.selectionIndex,
      format: this.selectionFormat,
      length: this.selectionLength,
      text: this.selectionText,
      unit: this.selectionUnit
    }

    return this._selection
  }

  get selectionIndex () {
    const result = document.getSelection().getRangeAt(0).startOffset

    this._selectionIndex = result

    return this._selectionIndex
  }

  get selectionFormat () {
    let result = 'untested selection format'

    this._selectionFormat = result

    return this._selectionFormat
  }

  get selectionLength () {
    let result = window.getSelection().getRangeAt(0).endOffset

    // Content editable give the same value to index and length when there is not a selection
    if (result === this._selectionIndex) {
      result = 0
    }

    this._selectionLength = result

    return this._selectionLength
  }

  get selectionText () {
    const fullText = this._contentEditable.innerText
    const index = this._selectionIndex
    const length = this._selectionLength
    let result = ''

    if (length === 0) {
      result = ''
    } else {
      result = fullText.substring(index, length)
    }

    this._selectionText = result

    return this._selectionText
  }

  get selectionUnit () {
    let result = 'untested selection unit'

    this._selectionUnit = result

    return this._selectionUnit
  }

  // Node
  get node () {
    this._node = {
      index: this.nodeIndex,
      hierarchy: this.nodeHierarchy,
      length: this.nodeLength,
      text: this.nodeText
    }

    return this._node
  }

  get nodeIndex () {
    let result = 0
    const nodes = this._contentEditable.childNodes
    const selectedNode = this._getNodeNumber()

    // Get the total length of previus node nodes
    let index = 0
    let length = 0

    for (index = 0; index < selectedNode; index++) {
      length = nodes[index].innerText.length

      result += length
    }

    this._nodeIndex = result

    return this._nodeIndex
  }

  get nodeHierarchy () {
    let result = 'Invalid node hierarchy'
    const eventType = this._event.type
    let nodeName = 'unknow node event'

    if (eventType === 'click') {
      nodeName = this._event.target.nodeName
    }
    if (eventType === 'keyup') {
      const nodes = this._contentEditable.childNodes
      const selectedNode = this._getNodeNumber()
      nodeName = nodes[selectedNode].nodeName
    }

    if (nodeName === 'H1') result = TextEditorStore.data.hierarchyList.heading_1
    if (nodeName === 'H2') result = TextEditorStore.data.hierarchyList.heading_2
    if (nodeName === 'H3') result = TextEditorStore.data.hierarchyList.heading_3
    if (nodeName === 'P') result = TextEditorStore.data.hierarchyList.paragraph
    if (nodeName === 'UL') result = TextEditorStore.data.hierarchyList.unordered_list
    if (nodeName === 'OL') result = TextEditorStore.data.hierarchyList.ordered_list

    this._nodeHierarchy = result

    return this._nodeHierarchy
  }

  get nodeLength () {
    const node = this._event.target
    const result = node.innerText.length

    this._nodeLength = result

    return this._nodeLength
  }

  get nodeText () {
    const node = this._event.target
    let result = 'unknow text source'
    let eventType = this._event.type

    if (eventType === 'click') {
      result = node.innerText
    } else {
      result = node.innerText.slice(0, -1)
    }

    this._nodeText = result

    return this._nodeText
  }

  _getNodeNumber () {
    let node = this._event.target
    let selectedNode = 0

    while ((node = node.previousSibling) != null) {
      selectedNode++
    }

    return selectedNode
  }
}

export default ContentEditableSelection
