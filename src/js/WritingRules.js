import TextEditorStore from '../stores/TextEditorStore'

class WritingRules {
  constructor () {
    this._TextEditorStore = TextEditorStore.data
    this._hierarchyList = this._TextEditorStore.hierarchyList
    this._event = {}
    this._node = {}
    this._selection = {}
  }

  get event () {
    this._event = TextEditorStore.data.event
  }

  get node () {
    this._node = TextEditorStore.data.node

    return this._node
  }

  get selection () {
    this._selection = TextEditorStore.data.selection

    return this._selection
  }

  // Heading length (action: warning)
  headingLength () {
    const node = this.node

    switch (node.hierarchy) {
      case this._hierarchyList.heading_1:
      case this._hierarchyList.heading_2:
      case this._hierarchyList.heading_3:
        const length = node.length
        const limit = 65

        if (length > limit) {
          alert(`Este título es muy largo.\nUn buen titular no debe pasar de ${limit} y el tuyo tiene ${length}.`)
        }
    }
  }

  // Horizontal space (action: stop)
  horizotnalSpace () {
    let result = false
    const node = this.node
    const selection = this.selection
    const text = this.node.text
    const croppedText = text.substr(node.index, selection.index)
    const previousCharacter = croppedText.charAt(croppedText.length - 1)

    console.log(`prev @${previousCharacter}@`)

    // For some weird reason this 2 whitespaces are considered different in code
    if (previousCharacter === ' ' || previousCharacter === ' ') {
      result = true
      alert('No debes insertar varios espacios en blanco.')
    }

    return result
  }

  // Vertical line (action: stop)
  verticalSpace () {
    let result = false
    const node = this.node
    const selection = this.selection

    // If the cursor is at the beginning of a new line
    if (selection.index === 0) {
      result = true
      alert('No debes insertar líneas vacías.')
    }

    return result
  }

  verticalSpaceDocumentTitle () {
    const node = this.node
    let result = false

    // Block the ENTER key entirely because we are at the document title
    if (node.hierarchy === this._hierarchyList.heading_1) {
      alert('No puedes usar otras líneas en en título.')
      result = true
    }

    return result
  }
}

export default WritingRules
