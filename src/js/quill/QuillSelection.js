import TextEditorStore from '../../stores/TextEditorStore'

class QuillSelection {
  constructor (quillInstance) {
    this._quill = quillInstance

    // For Text Editor
    this._selection = {}
    this._selectionIndex = 0
    this._selectionFormat = {}
    this._selectionLength = 0
    this._selectionText = ''
    this._selectionUnit = ''
    this._node = {}
    this._nodeIndex = ''
    this._nodeHierarchy = {}
    this._nodeLength = 0
    this._nodeText = 0

    // For File Generation
    this._content = {}
  }

  // Selection
  get selection () {
    if (this._quill.getSelection()) {
      this._selection = {
        index: this.selectionIndex,
        format: this.selectionFormat,
        length: this.selectionLength,
        text: this.selectionText,
        unit: this.selectionUnit
      }
    }

    return this._selection
  }

  get selectionIndex () {
    let result = this._quill.getSelection()

    this._selectionIndex = result.index

    return this._selectionIndex
  }

  get selectionFormat () {
    let result = this._quill.getFormat()

    if (Object.keys(result).length === 0) {
      result = 'unformatted text'
    }

    this._selectionFormat = result

    return this._selectionFormat
  }

  get selectionLength () {
    const result = this._quill.getSelection()

    this._selectionLength = result.length

    return this._selectionLength
  }

  get selectionText () {
    const result = this._quill.getText(this.selectionIndex, this.selectionLength)

    this._selectionText = result

    return this._selectionText
  }

  get selectionUnit () {
    let result = ''

    if (this._isEmptySpace()) result = TextEditorStore.data.unitList.empty_space
    if (this._isWord()) result = TextEditorStore.data.unitList.word
    if (this._isBlock()) result = TextEditorStore.data.unitList.block

    this._selectionUnit = result

    return this._selectionUnit
  }

  // Node
  get node () {
    if (this._quill.getSelection()) {
      this._node = {
        index: this.nodeIndex,
        hierarchy: this.nodeHierarchy,
        length: this.nodeLength,
        text: this.nodeText
      }
    }

    return this._node
  }
  get nodeIndex () {
    const index = this.selectionIndex
    const blot = this._quill.getLeaf(index)[0]
    const blotIndex = this._quill.getIndex(blot)

    this._nodeIndex = blotIndex

    return this._nodeIndex
  }

  get nodeHierarchy () {
    const index = this.selectionIndex
    const blot = this._quill.getLeaf(index)[0]
    const nodeName = blot.domNode.parentNode.nodeName
    let result = 'Invalid node hierarchy'

    if (nodeName === 'H1') result = TextEditorStore.data.hierarchyList.heading_1
    if (nodeName === 'H2') result = TextEditorStore.data.hierarchyList.heading_2
    if (nodeName === 'H3') result = TextEditorStore.data.hierarchyList.heading_3
    if (nodeName === 'P') result = TextEditorStore.data.hierarchyList.paragraph
    if (nodeName === 'UL') result = TextEditorStore.data.hierarchyList.unordered_list
    if (nodeName === 'OL') result = TextEditorStore.data.hierarchyList.ordered_list

    this._nodeHierarchy = result

    return this._nodeHierarchy
  }

  get nodeLength () {
    const index = this.selectionIndex
    const blot = this._quill.getLeaf(index)[0]
    const blotLength = blot.parent.cache.length - 1

    this._nodeLength = blotLength

    return this._nodeLength
  }

  get nodeText () {
    const result = this._quill.getText(this.nodeIndex, this.nodeLength)

    this._nodeText = result

    return this._nodeText
  }

  // Content
  get content () {
    const result = this._quill.getContents(0, this._quill.getLength)

    this._content = result

    return this._content
  }

  _isEmptySpace () {
    const index = this.selectionIndex
    const length = this.selectionLength
    let result = false

    if (length === 0) {
      // Point selection
      let prevChar = this._quill.getText(index - 1, 1)
      let nextChar = this._quill.getText(index, 1)

      result = prevChar === ' ' && (nextChar === ' ' || nextChar === '\n')
    } else {
      // Range Selection
      let selection = this._quill.getText(index, length)
      selection = selection.replace(/[ \n]/g, '')

      result = selection === ''
    }

    return result
  }

  _isWord () {
    const index = this.selectionIndex
    const length = this.selectionLength
    const blotIndex = this.nodeIndex
    const blotLength = this.nodeLength
    let result = false

    if (length === 0) {
      // Point selection
      let beforeSelection = this._quill.getText(blotIndex, index - blotIndex)
      let afterSelection = this._quill.getText(index, index + blotLength)

      beforeSelection = beforeSelection.split(/\W/).splice(-1)
      afterSelection = afterSelection.split(/\W/)[0]

      result = beforeSelection + afterSelection !== ''
    } else {
      // Range selection
      let rangeSelection = this._quill.getText(index, length)

      rangeSelection = rangeSelection.split(/\W/)[0]

      result = rangeSelection !== ''
    }

    return result
  }

  _isBlock () {
    const index = this.selectionIndex
    const length = this.selectionLength
    const blotIndex = this.nodeIndex
    const blotLength = this.nodeLength
    let result = false

    // The blotLength +1 at the end of this query is for accounting the new line after each paragraph
    result = blotIndex === index && (blotLength === length || blotLength + 1 === length)

    return result
  }
}

export default QuillSelection
