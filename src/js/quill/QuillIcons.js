import Quill from 'quill'

class QuillIcons {
  constructor () {
    let icons = Quill.import('ui/icons')

    icons['bold'] = "<span class='icon-bold'></span>"
    icons['italic'] = "<span class='icon-bold'></span>"
    icons['script']['super'] = "<span class='icon-super'></span>"
    icons['header']['2'] = "<span class='icon-title'></span>"
    icons['header']['3'] = "<span class='icon-subtitle'></span>"
    icons['clean'] = "<span class='icon-paragraph'></span>"
    icons['list']['ordered'] = "<span class='icon-list-numbered'></span>"
    icons['list']['bullet'] = "<span class='icon-list-bullet'></span>"
  }
}

export default QuillIcons
