const MenuBarStore = {
  data: {
    items: [
      {
        id: 1,
        panel: 'ThemePanel',
        icon: 'icon-theme',
        label: 'Cambiar tema'
      }
    ]
  }
}

export default MenuBarStore
