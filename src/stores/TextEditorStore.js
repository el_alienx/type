const TextEditorStore = {
  data: {
    debug: false,
    hierarchyList: {
      heading_1: 'heading_1',
      heading_2: 'heading_2',
      heading_3: 'heading_3',
      paragraph: 'paragraph',
      unordered_list: 'unordered list',
      ordered_list: 'ordered list'
    },
    formatList: {
      bold: 'bold',
      italic: 'italic',
      super: 'super',
      underlined: 'underlined'
    },
    unitList: {
      block: 'block',
      empty_space: 'empty_space',
      other: 'other',
      word: 'word'
    },
    scrolled: false,
    currentEditor: 'quill',
    selection: {
      index: 0,
      format: {},
      length: 0,
      text: '',
      unit: ''
    },
    node: {
      index: 0,
      hierarchy: '',
      length: 0,
      text: ''
    },
    contentEditable: {
      placeholder: 'Escribe tu título aquí'
    },
    quill: {
      parameters: {
        placeholder: 'Escribe o pega (⌘V) tu texto aquí. Presiona los botones de arriba para mejorar tu documento.',
        theme: 'bubble',
        modules: {
          toolbar: '.pop'
        }
      }
    }
  }
}

export default TextEditorStore
