const PanelContainerStore = {
  data: {
    currentPanel: ''
  },
  methods: {
    setPanel (panel) {
      PanelContainerStore.data.currentPanel = panel
    },
    closePanel () {
      PanelContainerStore.data.currentPanel = ''
    }
  }
}

export default PanelContainerStore
