const ThemeStore = {
  data: {
    themes: ['theme-serif', 'theme-sans-serif'],
    selectedTheme: 'theme-sans-serif',
    items: [
      {
        id: 1,
        theme: 'theme-sans-serif',
        label: 'Moderno',
        image: require('../../assets/images/panel/theme-panel/page-thumb-modern.jpg'),
        background: require('../../assets/images/panel/theme-panel/background-modern.jpeg')
      },
      {
        id: 2,
        theme: 'theme-serif',
        label: 'Clásico',
        image: require('../../assets/images/panel/theme-panel/page-thumb-classic.jpg'),
        background: require('../../assets/images/panel/theme-panel/background-classic.jpeg')
      },
      {
        id: 3,
        theme: 'theme-mix',
        label: 'Mix',
        image: require('../../assets/images/panel/theme-panel/page-thumb-mix.jpg'),
        background: require('../../assets/images/panel/theme-panel/background-mix.jpeg')
      }
    ]
  },
  methods: {
    setTheme (theme) {
      ThemeStore.data.selectedTheme = theme
    }
  }
}

export default ThemeStore
